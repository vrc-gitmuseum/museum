﻿using System;
using System.Collections;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using UnityEditor;
using UnityEngine;
using UnityEngine.Networking;

namespace IwashiBox
{
    public class UpdateChecker : Editor
    {
        [MenuItem("iwashibox/Update Check")]
        public static void UpdateCheck()
        {
            VersionInfo version = AssetDatabase.LoadAssetAtPath<VersionInfo>(AssetDatabase.FindAssets("l:IwashiBoxVersionInfo").Select(g => AssetDatabase.GUIDToAssetPath(g)).Where(p => p != null).FirstOrDefault());
            IEnumerator releases = GetReleases();
            while (releases.MoveNext()) { }
            if (releases.Current == null || version == null)
            {
                EditorUtility.DisplayDialog("Error", "現在のバージョンの取得に失敗しました。", "OK");
                return;
            }
            ReleaseData data = JsonUtility.FromJson<ReleaseData>("{\"data\": " + releases.Current.ToString() + "}");
            if (data == null || data.data == null || data.data.Length == 0)
            {
                EditorUtility.DisplayDialog("Error", "アップデートバージョンの取得に失敗しました。", "OK");
                return;
            }
            if (data.data[0].tag_name == version.version)
            {
                EditorUtility.DisplayDialog("No Update", "更新の必要はありません。", "OK");
                return;
            }
            Regex regex = new Regex(@"\(\/uploads\/(?<value>.*)\)");
            Match match = regex.Match(data.data[0].description);
            string url = "https://gitlab.com/iwashi_farm/iwashibox/uploads/" + match.Groups["value"].Value;
            string tmpPath = Path.GetTempPath();
            string packagePath = tmpPath + Path.GetFileName(url);
            IEnumerator download = DownloadPackage(url, packagePath);
            while (download.MoveNext()) { }
            if (!File.Exists(packagePath))
            {
                EditorUtility.DisplayDialog("Error", "UnityPackageを開けませんでした。", "OK");
                return;
            }
            try
            {
                AssetDatabase.ImportPackage(packagePath, true);
                File.Delete(packagePath);
            }
            catch
            {
                EditorUtility.DisplayDialog("Error", "UnityPackageを開けませんでした。", "OK");
                return;
            }
        }

        private static IEnumerator GetReleases()
        {
            using (UnityWebRequest request = UnityWebRequest.Get("https://gitlab.com/api/v4/projects/11673607/releases"))
            {
                request.SendWebRequest();
                while (!request.isDone) yield return null;
                if (request.isNetworkError || request.isHttpError) yield break;
                yield return request.downloadHandler.text;
            }
        }

        private static IEnumerator DownloadPackage(string url, string packagePath)
        {
            using (UnityWebRequest request = UnityWebRequest.Get(url))
            {
                request.SendWebRequest();
                while (!request.isDone)
                {
                    float progress = request.downloadProgress;
                    EditorUtility.DisplayProgressBar("Downloading...", (progress * 100).ToString("F2") + "%", progress);
                }
                if (request.isNetworkError || request.isHttpError) yield break;
                File.WriteAllBytes(packagePath, request.downloadHandler.data);
            }
        }

        [Serializable]
        private class ReleaseData
        {
            public Data[] data;
            [Serializable]
            public class Data
            {
                public string tag_name;
                public string description;
            }
        }
    }
}
